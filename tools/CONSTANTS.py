## Constats used in Robot

# Sleep Time between 2 Animations in Milliseconds
SLEEP_TIME = 500

# Clear Screen before each step (True/False)
CLEAR_SCREEN = True

# Single Step Modus (True/False)
SINGLE_STEP = False

# Pause on error
PAUSE_ON_ERROR = True

# Filename for World
FILE_NAME_ACTUAL_WORLD = "World.txt"
#FILE_NAME_ACTUAL_WORLD = "Testworld.txt"

# Frames Robot message will be shown
MESSAGE_LIFETIME = 4

# Chars representing free fields
CHAR_FREE = '.'

# Chars representing wall
CHAR_WALL = '#'

# Chars representing portal
CHAR_PORTAL = '@'

# Chars representing the room
CHARS_ROOM = [CHAR_FREE, CHAR_WALL, CHAR_PORTAL]

# Chars representing chars
LOWER_CHARS = [chr(ord('a') + x) for x in range(26)]
UPPER_CHARS = [chr(ord('A') + x) for x in range(26)]
DIGITS = [chr(ord('0') + x) for x in range(10)]
CHARS_CHARS = LOWER_CHARS + UPPER_CHARS + DIGITS

# special chars
CHAR_COOKIE = 'C'
CHAR_BOMB = 'B'

# Chars allowed in World
CHARS_WORLD = CHARS_ROOM + CHARS_CHARS

# Chars representing Robot directions
CHAR_ROBOT_RIGHT = '>'
CHAR_ROBOT_UP = '^'
CHAR_ROBOT_LEFT = '<'
CHAR_ROBOT_DOWN = 'v'

# Chars allowed for Robot
CHARS_ROBOT = [CHAR_ROBOT_RIGHT, CHAR_ROBOT_UP, CHAR_ROBOT_LEFT, CHAR_ROBOT_DOWN]

# All allowed chars
CHARS_ALLOWED = CHARS_WORLD + CHARS_ROBOT

# Map Row & Column to tuple index
ROW = 0
COL = 1

# Directions
DIRECTION_RIGHT = (0, 1)
DIRECTION_UP = (-1, 0)
DIRECTION_LEFT = (0, -1)
DIRECTION_DOWN = (1, 0)

# Directory mapping directions to chars
DIRECTIONS_TO_CHARS = {DIRECTION_RIGHT: CHAR_ROBOT_RIGHT,
                       DIRECTION_UP: CHAR_ROBOT_UP,
                       DIRECTION_LEFT: CHAR_ROBOT_LEFT,
                       DIRECTION_DOWN: CHAR_ROBOT_DOWN}

# Directory mapping chars to directions
CHARS_TO_DIRECTIONS = {CHAR_ROBOT_RIGHT : DIRECTION_RIGHT,
                       CHAR_ROBOT_UP : DIRECTION_UP,
                       CHAR_ROBOT_LEFT : DIRECTION_LEFT,
                       CHAR_ROBOT_DOWN : DIRECTION_DOWN}

# Sign for turning direction
R = -1
L = +1

