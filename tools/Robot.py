from time import sleep
from random import randint

from tools.CONSTANTS import *
from tools.World import World


class Robot:

    def __init__(self):
        # World
        self.world = World()
        # position
        self.position = (0, 0)
        # direction
        self.direction = (0, 0)
        # charge
        self.charge = []
        # message
        self.message = ""
        # message lifetime 
        self.message_lifetime = 0
        # error
        self.error = False

        self.find_robot_in_world()
        self.print()

    def find_robot_in_world(self):
        row = 0
        col = 0
        for line in self.world.grid:
            col = 0
            for char in line:
                if char in CHARS_ROBOT:
                    self.position = (row, col)
                    self.direction = CHARS_TO_DIRECTIONS.get(char)
                col = col + 1
            row = row + 1

    def position_in_front(self):
        return (self.position[ROW] + self.direction[ROW], self.position[COL] + self.direction[COL])

    def look(self):
        new_position = self.position_in_front()
        if self.world.has_char_at(new_position):
            return self.world.get_char_at(new_position)

    def move(self):
        if self.look() == CHAR_FREE:
            self.world.set_char_at(self.position, CHAR_FREE)
            self.position = self.position_in_front()
            self.world.set_char_at(self.position, DIRECTIONS_TO_CHARS.get(self.direction))
        elif self.look() == CHAR_PORTAL:
            old_position = self.position
            self.world.set_char_at(self.position, CHAR_FREE)
            self.position = self.find_next_door()
            turns = 0
            while self.look() != CHAR_FREE and turns < 4:
                # logic from turn(self, sign)
                self.direction = (-L * self.direction[COL], L * self.direction[ROW])
                turns += 1
            if turns < 4:
                self.position = self.position_in_front()
            else:
                self.position = old_position
                self.message = "Autsch, kein Platz, um nach Portalsprung nach (" \
                + str(self.find_next_door()[ROW]) + "," + str(self.find_next_door()[ROW]) \
                + ") nach vorne zu gehen!  :-("
                self.message_lifetime = MESSAGE_LIFETIME
                self.error = True
            self.world.set_char_at(self.position, DIRECTIONS_TO_CHARS.get(self.direction))
        else:
            self.message = "Autsch, kein Platz, um nach vorne zu gehen!  :-("
            self.message_lifetime = MESSAGE_LIFETIME
            self.error = True
        self.print()

    def turn(self, sign):
        # x_new         cos(sign * 90)  -sin(sign * 90)     x
        #           =                                    *
        # y_new         sin(sign * 90)  cos(sign * 90)      y
        #
        # x_new         0  -sign   x
        #           =            *
        # y_new         sign   0   y
        #
        # col_new       0  -sign   col
        #           =            *
        # row_new       sign   0   row
        #
        # col_new = -sign * row
        # row_new = +sign * col
        #
        self.direction = (-sign * self.direction[COL], sign * self.direction[ROW])
        self.world.set_char_at(self.position, DIRECTIONS_TO_CHARS.get(self.direction))
        self.print()

    def load(self):
        if not self.look() in CHARS_ROOM:
            new_position = self.position_in_front()
            self.charge.append(self.world.get_char_at(new_position))
            self.world.set_char_at(new_position, CHAR_FREE)
        else:
            self.message = "Nichts zum aufladen vor mir?!  :-("
            self.message_lifetime = MESSAGE_LIFETIME
            self.error = True
        self.print()

    def unload(self, thing):
        if self.look() != CHAR_FREE:
            self.message = "Schlechter Platz zum abladen!! :-("
            self.message_lifetime = MESSAGE_LIFETIME
            self.error = True
        elif not thing in self.charge:
            self.message = "%s:" % thing + " habe ich nicht geladen?!  :-("
            self.message_lifetime = MESSAGE_LIFETIME
            self.error = True
        else:
            self.charge.remove(thing)
            self.world.set_char_at(self.position_in_front(), thing)
        self.print()

    def print(self):
        self.world.print()
        print('Robots charge: ', end='')
        print(self.charge)
        if self.message != "":
            print(self.message)
            self.message_lifetime -= 1
            if self.message_lifetime == 0:
                self.message = ""
        if SINGLE_STEP or (PAUSE_ON_ERROR and self.error):
            self.message = ""
            self.error = False
            input("Continue with 'RETURN'")
        else:
            sleep(SLEEP_TIME/1000)

    def find_next_door(self):
        row = 0
        col = 0
        for line in self.world.grid:
            col = 0
            for char in line:
                position_in_front = self.position_in_front()
                # skip door_in
                if char == CHAR_PORTAL and (row != position_in_front[ROW] or col != position_in_front[COL]):
                    # return first door found
                    return (row, col)
                col = col + 1
            row = row + 1
        # return door_in, if no other door is found
        return self.position_in_front()


if __name__ == '__main__':
    print("Main in Robot")

r = Robot()


def move():
    r.move()


def turn(lr):
    r.turn(lr)


def look():
    return r.look()


def load():
    return r.load()


def unload(thing):
    return r.unload(thing)


def set_random_char(char):
    while True:
        random_row = randint(1,len(r.world.grid) - 2)
        random_col = randint(1,len(r.world.grid[0]) - 2)
        if r.world.get_char_at((random_row, random_col)) == CHAR_FREE:
            r.world.set_char_at((random_row, random_col),char)
            r.print()
            break;


def set_random_cookie():
    set_random_char(CHAR_COOKIE)


def set_random_wall():
    set_random_char(CHAR_WALL)


def set_random_robot():
    set_random_char(CHAR_ROBOT_UP)
