import os
from platform import system

from tools.CONSTANTS import *


def read_grid_from_file(grid):
    f = 0
    try:
        f = open(FILE_NAME_ACTUAL_WORLD, "r", encoding="utf-8")
        for line in f.readlines():
            # remove spaces
            line = line.lstrip(' ').rstrip(' ')
            # if line is no comment or empty
            if not (line.startswith(';') or line in [os.linesep]):
                row = []
                for char in line:
                    if char in CHARS_ALLOWED:
                        row.append(char)
                grid.append(row)
    finally:
        f.close()


class World:

    def __init__(self):
        # create grid as empty list
        self.grid = []
        read_grid_from_file(self.grid)

    def has_char_at(self, coordinate):
        return (coordinate[ROW] >= 0) and (coordinate[COL] >= 0) \
                and (coordinate[ROW] < len(self.grid)) and (coordinate[COL] < len(self.grid[coordinate[ROW]]))

    def set_char_at(self, coordinate, char):
        if self.has_char_at(coordinate):
            self.grid[coordinate[ROW]][coordinate[COL]] = char
        else:
            print("ERROR MESSAGE: No Char at ROW: %s" % coordinate[ROW] + " COL: %s" % coordinate[COL])

    def get_char_at(self, coordinate):
        if self.has_char_at(coordinate):
            return self.grid[coordinate[ROW]][coordinate[COL]]
        else:
            print("ERROR MESSAGE: No Char at ROW: %s" % coordinate[ROW] + " COL: %s" % coordinate[COL])

    def to_string(self):
        out_string = ""
        for line in self.grid:
            for char in line:
                out_string = out_string + char
            out_string = out_string + os.linesep
        return out_string

    def print(self):
        if CLEAR_SCREEN:
            if system() == "Linux":
                os.system("clear")
            elif system() == "Windows":
                os.system("cls")
        print(self.to_string())


if __name__ == '__main__':
    print("Main in World")


