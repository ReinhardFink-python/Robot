# Robot

A simple python framework to work with a "Robot" in the console.

Create a World in World.txt:

```python
Wall: #

Free: .

Cookie: C

Robot: ^ up, < left, v down & > right


##########
#........#
#........#
#........#
#.......C#
#........#
#........#
#........#
#.......^#
##########
```

in act.py you can control the robot with:

```python
move()          # move one step forward
look()          # return char in front of robot
turn(L)         # turn 90° left 
turn(R)         # turn 90° right
load()          # loads char in front of robot into robots bag
unload(char)    # if char in robots bag, unload it in front of robot

```
in tools/CONSTANTS.py you can control:


```python
## Constats used in Robot

# Sleep Time between 2 Animations in Milliseconds
SLEEP_TIME = 500

# Clear Screen before each step (True/False)
CLEAR_SCREEN = True

# Single Step Modus (True/False)
SINGLE_STEP = False

# Pause on error
PAUSE_ON_ERROR = True
```
